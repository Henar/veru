<?php

    function datosHunesco(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://examples.opendatasoft.com/api/records/1.0/search/?dataset=world-heritage-unesco-list&q=&rows=1052&facet=category&facet=country_en&facet=country_fr&facet=continent_en&facet=continent_fr");
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        
        $response = curl_exec($ch);
        
        $pics = json_decode($response, true);
        
        // foreach ($pics['records'] as $value) {
        // echo $value['fields']['name_fr']. '<br/>';
        // }

        //echo $pics['records'][0]['fields']['category'];
        // print_r($pics['records']);
        return $pics['records'];
    }

    $datosLocalizaciones=datosHunesco();

    if(count($datosLocalizaciones)>0){
        header("Content-type: text/xml");

        $dom=new DOMDocument('1.0');
        $nodo=$dom->createElement('markers');
        $nodoPadre=$dom->appendChild($nodo);

        for ($i=0; $i < count($datosLocalizaciones); $i++) {
            $nodo=$dom->createElement('marker');
            // print_r($datosLocalizaciones[$i]);
            // echo '<br/>';
            $nodoMarker=$nodoPadre->appendChild($nodo);

            $lugar=$datosLocalizaciones[$i]['fields']['country_en']. ', '. $datosLocalizaciones[$i]['fields']['continent_en'];

            $nodoMarker->setAttribute('id', $datosLocalizaciones[$i]['recordid']);
            $nodoMarker->setAttribute('nombre', $datosLocalizaciones[$i]['fields']['name_en']);
            $nodoMarker->setAttribute('tipo', $datosLocalizaciones[$i]['fields']['category']);
            $nodoMarker->setAttribute('longitud', $datosLocalizaciones[$i]['fields']['longitude']);
            $nodoMarker->setAttribute('latitud', $datosLocalizaciones[$i]['fields']['latitude']);
            $nodoMarker->setAttribute('direccion', $lugar);


        }
        echo $dom->saveXML();
    }

?>