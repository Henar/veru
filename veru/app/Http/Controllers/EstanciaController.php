<?php

namespace App\Http\Controllers;

use App\Models\Estancia;
use App\Models\Lugar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EstanciaController extends Controller
{
    public function index(){
        $estancias=Estancia::all();
        $estancias=Estancia::paginate(9);
		return view('estancias.index', compact('estancias'));
    }

    public function show(Estancia $estancia){
        
        $es=Estancia::findOrFail($estancia->id);
        return view('estancias.show', ["estancia"=>$es]);
    }

    public function create(){
        
        $lugares=Lugar::all();
        return view('estancias.create', compact('lugares'));
    }

    public function store(Request $request){
        $estancia=new Estancia();
        
        $estancia->hotel=$request->hotel;
        $estancia->precio=$request->precio;
        $estancia->lugar_id=$request->lugar_id;
        $estancia->esSostenible=$request->esSostenible;
        if(!empty($request->imagen) && $request->imagen->isValid()){
            Storage::disk('estancias')->delete($estancia->imagen);
            $estancia->imagen=$request->imagen->store('', 'estancias');
        }
        $estancia->save();

        return redirect()->route('viajes.index')->with('mensaje', "Estancia añadida con éxito");

    }


    public function edit(Estancia $estancia){
        
        $estancia=estancia::findOrFail($estancia->id);
        return view('estancias.edit', compact('estancia'));
    }

    public function update(Request $request, Estancia $estancia){
       
        $estancia->hotel=$request->hotel;
        $estancia->precio=$request->precio;
        $estancia->esSostenible=$request->esSostenible;

        if(!empty($request->imagen) && $request->imagen->isValid()){
            Storage::disk('estancias')->delete($estancia->imagen);
            $estancia->imagen=$request->imagen->store('', 'estancias');
        }
       
        $estancia->save();

        return redirect()->route('estancias.show', $estancia)->with('mensaje', "Estancia actualizada con éxito");


    }


    public function destroy(Estancia $estancia){
        $estancia->delete();
        return redirect()->route('viajes.index')->with('mensaje', 'Estancia eliminada correctamente.');
    }
}