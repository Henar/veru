<?php

namespace App\Http\Controllers;

use App\Models\Estancia;
use App\Models\Lugar;
use App\Models\Reserva;
use App\Models\Transporte;
use Illuminate\Http\Request;
use App\Traits\ViajeTrait;

class ReservaController extends Controller
{

    public function index(){
       
        $idUsu= auth()->id();
        //$reservas=Reserva::all()->where('cliente_id','=',$idUsu);
        $reservas=Reserva::all()->where('cliente_id', $idUsu);
        //$reservas=Reserva::paginate(9)->where('cliente_id', $idUsu);
        return view('reservas.index', ["reservas"=>$reservas]);
    }

    public function show(Reserva $reserva){
        
        $res=Reserva::findOrFail($reserva->id);
        return view('reservas.show', ["reserva"=>$res]);
    }

    public function edit(Reserva $reserva){
        
        $reserva=Reserva::findOrFail($reserva->id);
        $hoteles=Estancia::all();
        //$users = User::where("estado","=",1)->select("id","username") Para que sólo me duevuelva unos campos
        //return $hoteles;
        $transportes=Transporte::all();
        return view('reservas.edit', compact('reserva', 'hoteles', 'transportes'));
    }

    public function update(Request $request, Reserva $reserva){
        $reserva->estancia_id=$request->hotel;
        $reserva->transporte_id=$request->transporte;
        $reserva->fechaReserva=$request->fechaEntrada;
        $reserva->fechaFinReserva=$request->fechaSalida;
       
        $reserva->save();

        return redirect()->route('reservas.show', $reserva)->with('mensaje', "Reserva actualizada con éxito");


    }

    public function create(Lugar $viaje){
        //return $viaje;
        //$viaje=Lugar::findOrFail($viaje->id);
        $lugares=Lugar::all();
        $hoteles=Estancia::all();
        $transportes=Transporte::all();
        return view('reservas.create', compact('viaje', 'hoteles', 'transportes', 'lugares'));
    }

    public function store(Request $request){
        //return $request->all();
        $reserva=new Reserva();
        $idUsu= auth()->id();
        //return $idUsu;
        $fechaActual=date("d-m-Y");

        $precioTotal=0;

        $arrayValH=explode(",", ($request->hotel));
        $idH=$arrayValH[0];
        $hotel_id=intval(substr($idH, 6));
        
        $esta=Estancia::findOrFail($hotel_id);

        $arrayValT=explode(",", ($request->transporte));
        $idT=$arrayValT[0];
        $transporte_id=intval(substr($idT, 6));
        //return $hotel_id;

        $tra=Transporte::findOrFail($transporte_id);
        $preEsta=$esta->precio;
        // return $preEsta;
        
        $preTra=$tra->precio;
        $precioDes=$request->precioDestino;
        // return $precioDes;
        
        $fecha1=date_create($request->fechaEntrada);
        $fecha2=date_create($request->fechaSalida);
        $dias=$fecha1->diff($fecha2)->format('%R%a');
        // return $dias;

        $totHot=$preEsta*$dias;
        $precioTotal=$preTra + $totHot + $precioDes;


        $reserva->cliente_id= $idUsu;
        $reserva->estancia_id=$hotel_id;
        $reserva->transporte_id=$transporte_id;
        $reserva->fechaReserva=$request->fechaEntrada;
        $reserva->fechaFinReserva=$request->fechaSalida;
        $reserva->totalReserva=$precioTotal;
        return $precioTotal;
        $reserva->save();

        // return var_dump($reserva);
        return redirect()->route('reservas.index')->with('mensaje', "Reserva añadida con éxito");

    }
    
    // Añadir estancia al destino

    public function destroy(Reserva $r){
        // return $r;
        // if($r->delete())
        //     return 'bien';
        // else
        //     return 'mal';
        $r->delete();
        return redirect()->route('reservas.index')->with('mensaje', 'Reserva anulada correctamente');
    }

}
