<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estancia extends Model
{
    use HasFactory;

    protected $table='estancias';

    protected $fillable = [
        'hotel',
        'precio',
        'imagen',
        'lugar_id',
        'esSostenible',
    ];

    public function reservas(){
        return $this->hasMany(Reserva::class);
    }

    public function lugar(){
        return $this->belongsTo(Lugar::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

}
