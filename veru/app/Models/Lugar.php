<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    use HasFactory;

    protected $table='lugares';

    protected $fillable = [
        'pais',
        'ciudad',
        'precio',
        'imagen',
        'descripcion'
    ];

    public function estancias(){
        return $this->hasMany(Estancia::class);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public static function imgagenFlickr($ciudad){
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=46d7c83f534201381005d17eeae9f3b0&format=json&tags=". 
                $ciudad. "&per_page=1&page=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    
        $response= curl_exec($ch);
        // $respuesta=json_decode($response);
        
        $datos=explode('{', $response);
        
        $valoresImagen=[];
        
        if(count($datos)>3){
            $separa=explode(',', $datos[3]);
            
            $numId=(explode(':', $separa[0]))[1];
            $id=substr($numId, 1, -1);
    
            $numSecret=(explode(':', $separa[2]))[1];
            $secret=substr($numSecret, 1, -1);
            
            $numServer=(explode(':', $separa[3]))[1];
            $server=substr($numServer, 1, -1);
    
            $valoresImagen=[
                'server'=>$server, 
                'id'=>$id, 
                'secret'=>$secret
            ];
        }
    
        return $valoresImagen;
    }

}
