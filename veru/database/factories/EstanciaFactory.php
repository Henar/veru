<?php

namespace Database\Factories;

use App\Models\Estancia;
use App\Models\Lugar;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EstanciaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Estancia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $dir='public/assets/imagenes';
        $width='400';
        $height='350';
        $fEntrada=$this->faker->date($format = 'Y-m-d', $min = 'now');

        $nombre=$this->faker->company;
        return [
            'hotel'=>$nombre,
            'precio'=>$this->faker->randomFloat($nbMaxDecimals = 2, $min = 15, $max = 1000),
            'imagen'=>$this->faker->image($dir, $width, $height, null, false), 
            'slug'=>Str::slug($nombre, '-'),
            'esSostenible'=>$this->faker->boolean,
            'lugar_id'=>Lugar::all()->random()->id,
        ];
    }
}
