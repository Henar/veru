<?php

namespace Database\Factories;

use App\Models\Lugar;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LugarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lugar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $ciudad=$this->faker->unique()->city;
        $pais=$this->faker->country;
        $slug=Str::slug($ciudad, '-');
        $imagenRuta=Lugar::imgagenFlickr($slug);
        $imagen="";
        if(!empty($imagenRuta)){
            $imagen ="https://live.staticflickr.com/{$imagenRuta['server']}/{$imagenRuta['id']}_{$imagenRuta['secret']}_w.jpg";
        }else{
            $imagenRuta=Lugar::imgagenFlickr($pais);
            if(empty($imagenRuta)){
                $imagenRuta=Lugar::imgagenFlickr('viaje');
                $imagen="https://live.staticflickr.com/{$imagenRuta['server']}/{$imagenRuta['id']}_{$imagenRuta['secret']}_w.jpg";
            }else{
                $imagen="https://live.staticflickr.com/{$imagenRuta['server']}/{$imagenRuta['id']}_{$imagenRuta['secret']}_w.jpg";
            }
        }
        

        return [
            'pais'=>$pais,
            'ciudad'=>$ciudad,
            'descripcion'=>$this->faker->realText($maxNbChars = 700, $indexSize = 2),
            //(El false es para que no ponga la ruta antes del nombre de la imagen)
            //El null es el texto que le da, es como una marca de agua
            'imagen'=>$imagen, 
            'slug'=>$slug,
            'precio'=>$this->faker->randomFloat($nbMaxDecimals = 2, $min = 15, $max = 1000),
            
        ];
    }

       
}
