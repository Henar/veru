<?php

namespace Database\Seeders;

use App\Models\Rol;
use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol=new Rol();
        $rol->rol='admin';
        $rol->save();

        $rol=new Rol();
        $rol->rol='usuario';
        $rol->save();
    }
}
