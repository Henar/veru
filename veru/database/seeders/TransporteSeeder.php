<?php

namespace Database\Seeders;

use App\Models\Transporte;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TransporteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

   
 

    public function run()
    {
        $vehiculos=array('Avión', 'Barco', 'Tren', 'Coche','Autobus', 'Taxi', 'Avioneta', 'Bicicleta', 'Caravana', 'Moto');
        for ($i=0; $i < count($vehiculos); $i++) { 
            $transpor = new Transporte();
            $transpor->nombre = $vehiculos[$i];
            if($i>0 && $i%2==0){
                $transpor->esSostenible=0;
            }else{
                $transpor->esSostenible=1;
            }
            $transpor->slug = Str::slug($vehiculos[$i], '-'); 
            $transpor->precio=(mt_rand (20*10, 300*10) / 10);
            $transpor->save();
        }
        $this->command->info('Tabla transportes inicializada con datos');
    }
}
