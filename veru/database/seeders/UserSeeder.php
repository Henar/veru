<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=new User();
        $user->nombre="Henar";
        $user->apellidos=" ";
        $user->rol_id=1;
        $user->email = "henar@gmail.com";
        $user->password = bcrypt("1234");
        $user->numTarjeta=123;
        $user->save();

        $user2=new User();
        $user2->nombre="prueba";
        $user2->apellidos=" Prueba";
        $user2->rol_id=1;
        $user2->email = "pruba@gmail.com";
        $user2->password = bcrypt("prueba");
        $user2->numTarjeta=456;
        $user2->save();
    }
}
