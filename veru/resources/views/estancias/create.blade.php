@extends('layouts.master')

@section('titulo')
    Añadir Estancia
@endsection

@section('contenido')
    
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                @if (Auth::check() && Auth::user()->rol_id==1)
                <div class="card-header text-center">
                    {{-- <h4>{{$viaje}}</h4> --}}
                    Añadir una nueva estancia
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('estancias.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <br>
                        
                        <div class="form-group">
                            <label for="lugar_id">Introduzca el lugar al que quiera añadir la estancia</label><br>
                            
                            @error('lugar_id')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <select name="lugar_id" id="lugar_id" class="form-control" required>
                                @foreach ($lugares as $lugar)
                                    <option value="{{$lugar->id}}">{{$lugar->ciudad}}, {{$lugar->pais}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        
                        <br>
                        
                        <div class="form-group">
                            <label for="hotel">Introduzca el nombre del hotel que quiera añadir</label><br>
                            
                            @error('hotel')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                                <input type="text" name="hotel" id="hotel" class="form-control" value="{{old('hotel')}}" required>
                                                                
                            </select>
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="precio">Introduzca el precio del hotel que quiera añadir</label><br>
                            
                            @error('precio')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                                <input type="number" name="precio" id="precio" class="form-control" step="0.01" value="{{old('precio')}}" required>
                                
                            </select>
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="esSostenible">¿Es sostenible el hotel?</label><br>
                            
                            @error('esSostenible')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="radio" id="s" name="esSostenible" value="0">
                            <label for="s">Si</label>
                       
                           <input type="radio" id="n" name="esSostenible" value="1">
                           <label for="n">No</label>
                                                                
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="imagen">Introduzca la foto del hotel</label><br>
                            
                            @error('imagen')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <input type="file" name="imagen" id="imagen" required/>
                        </div>
                        <br>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir estancia</button>
                        </div>
                    </form>
                </div>
                @else
                <div class="card-body text center" style="padding:30px">
                    <h2>No puede acceder a esta página</h2>
                    <br>
                    <script>
                        document.write('<a href="' + document.referrer + '">Volver</a>');
                    </script>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection