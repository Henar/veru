@extends('layouts.master')

@section('titulo')
    Reserva
@endsection

@section('contenido')
    @php
        $fechaActual=date('Y-m-d');
    @endphp

    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                @if (Auth::check() && Auth::user()->rol_id==1)
                    <div class="card-header text-center">
                        Modificar la Estancia de <b>{{$estancia->hotel}}</b>
                    </div>
                    <div class="card-body" style="padding:30px">
                        <form action="{{ route('estancias.update', $estancia)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('put')

                            <br>

                            <div class="form-group">
                                <label for="hotel">Introduzca nuevo nombre del hotel</label><br>
                                
                                @error('hotel')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <input type="text" name="hotel" id="hotel" class="form-control" step="0.01" value="{{old('hotel', $estancia->hotel)}}" required>
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="precio">Introduzca nuevo precio para el hotel {{$estancia->hotel}}</label><br>
                                
                                @error('precio')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <input type="number" name="precio" id="precio" class="form-control" step="0.01" value="{{old('precio', $estancia->precio)}}" required>
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="esSostenible">¿Es sostenible el hotel?</label><br>
                                
                                @error('esSostenible')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <input type="radio" id="s" name="esSostenible" value="0">
                                <label for="s">Si</label>
                        
                            <input type="radio" id="n" name="esSostenible" value="1">
                            <label for="n">No</label>
                                                                    
                                </select>
                            </div>
                            <br>

                            <div class="form-group">
                                <label for="imagen">Introduzca una nueva foto del hotel</label><br>
                                
                                @error('imagen')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror

                                <input type="file" name="imagen" id="imagen"/>
                            </div>
                            <br>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Modificar estancia</button>
                            </div>
                        </form>
                    </div>
                @else
                <div class="card-body text center" style="padding:30px">
                    <h2>No puede acceder a esta página</h2>
                    <br>
                    <script>
                        document.write('<a href="' + document.referrer + '">Volver</a>');
                    </script>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection