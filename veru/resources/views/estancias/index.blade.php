@extends('layouts.master')

@section('titulo')
    Estancias
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <h3 style="text-align:center">Mostrando todas las posibles estancias</h3>
        <div class="album py-5 bg-light">
            <div class="container">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    @foreach($estancias as $estancia)
                        <div class="col">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <br>
                                    <a href="{{ route('estancias.show', $estancia)}}" style="text-decoration:none; color:black">    
                                        <h4 style="min-height:45px;margin:5px 0 10px 0" class="card-text">{{$estancia->hotel}}, en {{$estancia->lugar->ciudad}} ({{$estancia->lugar->pais}})</h4>
                                        
                                        <img src="{{asset('assets/imagenes/')}}/{{$estancia->imagen}}" alt="Imagen del destino {{$estancia->hotel}}" width="100%" height="100%">
                                    
                                    </a>

                                    <br><br>
                                </div>
                            </div>
                        </div>   
                    @endforeach
                </div>
            </div>
        </div>
        <br>
        {{$estancias->links()}}
    </form>
    
@endsection