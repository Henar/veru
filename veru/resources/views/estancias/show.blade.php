@extends('layouts.master')

@section('titulo')
    Estancias
@endsection

@section('contenido')
    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif

    <script>

        addEventListener('load', inicio, false);
        function inicio(){
            let caja1=document.getElementById('texto');
            let caja2 = document.getElementById('image');

            caja1.addEventListener('mouseover', function(){
                this.style.display="none";
                caja2.style.display="block";
            }, false);

            caja1.addEventListener('mouseout', function(){
                this.style.display="block";
                caja2.style.display="none";
            }, false);


        }
    </script>
    
    <form method="POST" action="{{ route('estancias.destroy', $estancia)}}">
        @csrf
        @method('delete')

        <h1 style="margin-left: 25%">{{$estancia->hotel}}</h1>
        <div class="row">
            <br><br>
            <div class="col-sm-3">
               <img src="{{asset('assets/imagenes/')}}/{{$estancia->imagen}}" width="100%" alt="Imagen del hotel {{$estancia->hotel}}"/>

            </div>
            <br>
            <div class="col-sm-9">
                <h3>Localización: </h3>
                <h5>({{$estancia->lugar->ciudad}}, {{$estancia->lugar->pais}})</h5>

                <br>
                <div id="texto">
                    <p>Mostar imagen del destino (Pase el ratón por encima)</p>   
                </div>
                <div id="image" style="display:none;">
                    @if(substr($estancia->lugar->imagen, 0, 8)==("https://"))
                        <img src="{{$estancia->lugar->imagen}}" width="22%" alt="Imagen del destino {{$estancia->lugar->ciudad}}"/>
                    @else
                        <img src="{{asset('assets/imagenes/')}}/{{$estancia->lugar->imagen}}" width="22%" alt="Imagen del destino {{$estancia->lugar->ciudad}}"/>
                    @endif
                </div>

                @if($estancia->esSostenible==0)
                    <h4 class="text-success">El hotel es amigable con el medio ambiente</h4>
                @endif

                <br/><br><br><br><br>
                @if (Auth::check() && Auth::user()->rol_id==1)
                    <a class="btn btn-warning" name="editar" style="margin-right: 7px" href = '{{ route('estancias.edit', $estancia)}}'>Editar</a>
                @endif
                
                <a class="btn btn-light btn-outline-dark" name="volver" href = '{{ route('viajes.index')}}' style="margin-left: 7px">Volver a la página de inicio</a>
                <a href="{{ route('viajes.show', $estancia->lugar)}}" class="btn btn-success" style="margin-left: 7px">Ir al destino</a>
                
                @if (Auth::check() && Auth::user()->rol_id==1)
                    <button class="btn btn-danger" name="borrar" style="margin-right: 7px" type="submit">Borrar estancia</button>
                    {{-- Preguntar si esta seguro si no cancelar la accion y si sí eliminar --}}
                @endif
            </div>
        </div>
       
    </form>
    
@endsection