<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" >
    <link href="{{ url('/assets/css/estilo.css') }}" rel="stylesheet" >

    {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="http://momentjs.com/downloads/moment.min.js"></script>
    
    

    <title>@yield('titulo')</title>
  </head>
  <body>
    
    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>

    @include('layouts.partials.navbar')
    
    <div class="contenido">
      <div class="container-fluid">
          @yield('contenido')
      </div>
      
      <div class='main'>
        <footer class="page-footer font-small bg-dark">
          <br>
          <img src="{{asset('assets/imagenes/logo.png')}}" alt="Logo de VERU" id="logoFooter" width="3%">
          <div class="footer-copyright text-center py-3 text-white">© 2021 Copyright:
            <a href="https://veru.com/"> VERU</a>
          </div>
        
        </footer>
      </div>
    </div>
  </body>
</html>