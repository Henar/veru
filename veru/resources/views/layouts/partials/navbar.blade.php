<nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background:#FF9671">
  <div class="container-fluid">
  <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/imagenes/logo.png')}}" alt="Logo de VERU" width="35%"></a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">

      <li class="nav-item">
        <a href="{{route('viajes.index')}}" class="nav-link {{ request()->routeIs('viajes.*') && !request()->routeIs('viajes.create')? ' active' : ''}}">Listado de destinos</a>
      </li>

      {{-- <li class="nav-item">
        <a href="{{route('lugares.index')}}" class="nav-link {{ request()->routeIs('lugares.index')? ' active' : ''}}">Lugares de interés</a>
      </li> --}}

      @if(Auth::check())

      <li class="nav-item">
        <a href="{{route('reservas.index')}}" class="nav-link {{ request()->routeIs('reservas.index')? ' active' : ''}}">Mostrar mis reservas</a>
      </li>

      @if(Auth::user()->rol_id==1)  
        
        <li class="nav-item">
            <a href="{{route('viajes.create')}}" class="nav-link {{ request()->routeIs('viajes.create')? ' active' : ''}}">Añadir un nuevo destino</a>
        </li>

        <li class="nav-item">
          <a href="{{route('estancias.create')}}" class="nav-link {{ request()->routeIs('estancias.create')? ' active' : ''}}">Añadir estancia</a>
        </li>

        {{-- <li class="nav-item">
          <a href="{{ route('register') }}" class="nav-link {{ request()->routeIs('register')? ' active' : ''}}">Añadir un usuario</a>
        </li> --}}
        
      @endif 

      @endif 

      <li class="nav-item">
        <a href="{{route('estancias.index')}}" class="nav-link {{ request()->routeIs('estancias.index') ? ' active' : ''}}">Listado de las distintas estancias</a>
      </li>

    </ul>
    
    
        <form class="d-flex">
          <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar una ciudad" aria-label="Buscar">

          <script>

            function generateSlug (value) {
              let valor;
              const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u', 'ñ':'n', 'à':'a','è':'e','ì':'i','ò':'o','ù':'u', 'â':'a','ê':'e','î':'i','ô':'o','û':'u',
                   'Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U', 'Ñ':'N', 'À':'A','È':'E','Ì':'I','Ò':'O','Ù':'U', 'Â':'A','Ê':'E','Î':'I','Ô':'O','Û':'U'};
              valor=value.split('').map( letra => acentos[letra] || letra).join('').toString();	
              return valor.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
            };

            $('#busqueda').autocomplete({
              source: function(request, response){
                $.ajax({
                  type:'POST',
                  url:"{{url('viajes/busquedaAjax')}}",
                  dataType:'json',
                  data:{
                    "_token": "{{csrf_token()}}",
                    "infoFormulario":request['term'],
                  },
                  

                   success:function(data){
                      response(data)
                    }, 
                  
                    error : function(xhr, status){
                      alert('Ha ocurrido un error')
                  }
                });
              },
              position:{
                my: "left+0 top+10",
              }, 
              select:function(event, ui){
                //alert(generateSlug(ui.item.value));
                //alert(window.location.origin)
                window.location=window.location.origin + "/viajes/" + generateSlug(ui.item.value);
              }
           });

          </script>
        </form>
        
        @if(Auth::check() )
        <ul class="navbar-nav navbar-right">
          <li class="nav-item font-weight-bold">
            <a href="{{ route('profile.show') }}"  class="nav-link h5">
               {{auth()->user()->nombre}}
            </a>
          </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  class="nav-link"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" >
                    <span class="glyphicon glyphicon-off"></span>
                    Cerrar sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    @else
        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a href="{{ route('register') }}" class="nav-link">Registrarse</a>
          </li>
            <li class="nav-item">
              <a href="{{url('login')}}" class="nav-link">Login</a>
            </li>
        </ul>
    @endif 
  </div>
</div>
</nav>




