@extends('layouts.master')

@section('titulo')
    Lugares de interés
@endsection

@section('contenido')

<body>

  <div id="mapa"></div>

  <script>
    var customLabel = {
      instituto: {
        label: 'I'
      },
      colegio: {
        label: 'C'
      }
    };

      function initMap() {
      var map = new google.maps.Map(document.getElementById('mapa'), {
        center: new google.maps.LatLng(40.432643430456444, -3.702890959242694),
        zoom: 6,
        mapTypeId: 'hybrid'
      });
      var infoWindow = new google.maps.InfoWindow;

        
          //downloadUrl('http://127.0.0.1/GenerarLugaresMapa/generarManualmente.php', function(data) {
          //var xml = data.responseXML;
          function(data){
          var xml = $marcadores;
          var markers = xml.documentElement.getElementsByTagName('marker');
          Array.prototype.forEach.call(markers, function(markerElem) {
            var id = markerElem.getAttribute('id');
            var name = markerElem.getAttribute('nombre');
            var address = markerElem.getAttribute('direccion');
            var type = markerElem.getAttribute('tipo');
            var point = new google.maps.LatLng(
                parseFloat(markerElem.getAttribute('latitud')),
                parseFloat(markerElem.getAttribute('longitud')));

            var infowincontent = document.createElement('div');
            var strong = document.createElement('strong');
            strong.textContent = name
            infowincontent.appendChild(strong);
            infowincontent.appendChild(document.createElement('br'));

            var text = document.createElement('text');
            text.textContent = address
            infowincontent.appendChild(text);
            infowincontent.appendChild(document.createElement('br'));

            var enlace = document.createElement('a');
            enlace.textContent = "Ver más información";
            //enlace.href= `viajes/sitios-interes/` + id;
          //   alert(enlace.href);

            infowincontent.appendChild(enlace);

            var icon = customLabel[type] || {};
            var marker = new google.maps.Marker({
              map: map,
              position: point,
              label: icon.label
            });
            marker.addListener('click', function() {
              infoWindow.setContent(infowincontent);
              infoWindow.open(map, marker);
            });
          });
        });
      }



    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function doNothing() {}
  </script>
  <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2eIAGqTUAsa_-qnNeAcwMNFBh4vYx8Kg&callback=initMap">
  </script>
@endsection
