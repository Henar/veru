@extends('layouts.master')

@section('titulo')
    Hacer Reserva
@endsection

@section('contenido')
    @php
        $fechaActual=date('Y-m-d');
    @endphp

    @php

        
        $url=$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
        $resultado=parse_url($url, PHP_URL_PATH);
        //echo $resultado;
        // echo $_GET['lugar'];
        $separado=explode('/', $resultado);
        //print_r($separado);
        $lugar=$separado[3];
        //echo "Lugar". $lugar;
        
        $lugarObj;
        foreach ($lugares as $lug) {
            if($lug->slug==$lugar){
                $lugarObj=$lug;
            }
        }
        // echo($lugarObj->descripcion);

    @endphp

        <script>
            //addEventListener('load', calcularTotal, false);
            
            function calcularTotal(){
                let precDestino="<?php echo ($lugarObj->precio); ?>";
                precDestino=parseFloat(precDestino);

                let selecH=document.getElementById("hotel").value;
                let arrayValH=selecH.split(",");
                let stringPrecH=arrayValH[2];
                let precH=parseFloat(stringPrecH.substr(9));
                
                let selecT=document.getElementById("transporte").value;
                let arrayValT=selecT.split(",");
                let stringPrecT=arrayValT[3];
                let precT=parseFloat(stringPrecT.substr(9));
                // alert(precT)
                
                let fechaEntra=new Date(document.getElementById("fechaEntrada").value);
                let fechaSale=new Date(document.getElementById("fechaSalida").value);
                
                // let dias=fechaSale.diff(fechaEntra, 'days');
                let difeTiempoMs=Math.abs(fechaSale - fechaEntra);
                let dias=Math.ceil(difeTiempoMs / (1000 * 60 * 60 * 24)); 
                
                //let precH=selecH[precio];

                // alert(typeof(dias))
                // alert(typeof(selecH));
                // alert(precH+precDestino)
                
                // alert(selecH);
               
                if(isNaN(precT)){
                    precT=0;
                }
                if(isNaN(precH)){
                    precH=0;
                }
                if(isNaN(dias)){
                    dias=0;
                }
                // alert(precH*dias)
                let total=precDestino + precT + (precH*dias);

                // let id=arrayValH[0];
                // alert(id.substr(6))
                let head=document.getElementById("precDestino");
                head.innerHTML=`El precio total del viaje es ${total}€`;
                document.getElementById("info").innerHTML=`El precio del destino es <b>${precDestino}€</b>`; 

            }

        </script>

        <div class="row">
            <div class="offset-md-3 col-md-6">
                <div class="card">
                    <div class="card-header text-center">
                        {{-- <h4>{{$viaje}}</h4> --}}
                        Crear la Reserva para <b>{{$lugarObj->ciudad}} ({{$lugarObj->pais}})</b>
                    </div>
                    <div class="card-body" style="padding:30px">
                        <form action="{{ route('reservas.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <br>
                            <input name="destino" type="hidden" value="{{$lugarObj->id}}">
                            {{-- <div class="col-sm-3">
                                <img src="{{asset('assets/imagenes/')}}/{{$valores['"imagen"']}}" style="height:100%"/>
                            </div> --}}

                            <div class="form-group">
                                <h4 id="precDestino" value="{{$lugarObj->precio}}">El precio del destino es {{$lugarObj->precio}}€</h4>
                                
                            </div>
                            <br>
                            <div class="form-group">
                                <p id="info"></p>
                            </div>

                            <div class="form-group">
                                <label for="hotel">Introduzca el lugar donde se quiera hospedar</label><br>
                                
                                @error('hotel')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                {{-- <p>{{$hoteles[0]->id}}</p> --}}
                                {{-- <p>{{$hoteles[0]}}</p> --}}
                                <select name="hotel" id="hotel" class="form-control" onchange="calcularTotal()" required>
                                    @foreach ($hoteles as $hotel)
                                        @if ($hotel->lugar_id==$lugarObj->id)
                                            @if ($hotel->esSostenible==0)
                                                <option value="{{$hotel}}" class="bg-success text-light hotelOpt" name="hotelOpt">{{$hotel->hotel}}  ({{$hotel->precio}}€/ noche)</option>
                                            @else
                                                <option value="{{$hotel}}" class="hotelOpt" name="hotelOpt">{{$hotel->hotel}}  ({{$hotel->precio}}€/ noche)</option>
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="fechaEntrada">Fecha Entrada al Hotel</label>
                                
                                @error('fechaEntrada')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <input type="date" name="fechaEntrada" id="fechaEntrada" class="form-control" min="{{$fechaActual}}" value="{{old('fechaEntrada')}}"  onchange="calcularTotal()" equired>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="fechaSalida">Fecha Salida del Hotel</label>
                                
                                @error('fechaSalida')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror

                                <script>
                                    $(document).ready(function(){
                                        $("#fechaSalida").click(function(){
                                            let valorFecha=document.getElementById('fechaEntrada').value;
                                            let etiqueta=document.getElementById('fechaSalida');
                                            etiqueta.setAttribute("min", valorFecha);
                                        });
                                    });
                                </script>
                                
                                <input type="date" name="fechaSalida" id="fechaSalida" class="form-control" value="{{old('fechaSalida')}}"  onchange="calcularTotal()" required>
                            </div> 
                            <br>
                            <div class="form-group">
                                <label for="transporte">Introduzca el medio de transporte con el que quiera viajar</label><br>
                                
                                @error('transporte')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                {{-- <p>{{$transportes[0]}}</p> --}}
                                <select name="transporte" id="transporte" class="form-control" onchange="calcularTotal()" required>
                                    @foreach ($transportes as $transporte)
                                        @if ($transporte->esSostenible==0)
                                            <option value="{{$transporte}}" class="bg-success text-light">{{$transporte->nombre}}  ({{$transporte->precio}}€)</option>
                                        @else
                                            <option value="{{$transporte}}">{{$transporte->nombre}}  ({{$transporte->precio}}€)</option>
                                        @endif   
                                    @endforeach
                                </select>
                                
                            </div>
                            <br>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir reserva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

       
@endsection