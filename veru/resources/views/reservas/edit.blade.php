@extends('layouts.master')

@section('titulo')
    Reserva
@endsection

@section('contenido')
    @php
        $fechaActual=date('Y-m-d');
    @endphp
   
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                 @if ($reserva->cliente_id== Auth::user()->id)
                <div class="card-header text-center">
                    Modificar la Reserva de <b>{{$reserva->estancia->lugar->ciudad}} ({{$reserva->estancia->lugar->pais}})</b>
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('reservas.update', $reserva)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <div class="form-group">
                            <label for="hotel">Introduzca el lugar donde se quiera hospedar</label><br>
                            
                            @error('hotel')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            <select name="hotel" id="hotel" class="form-control"  required>
                                @foreach ($hoteles as $hotel)
                                    @if ($hotel->lugar_id==$reserva->estancia->lugar->id)
                                        @if ($hotel->esSostenible==0)
                                            <option value="{{$hotel->id}}" class="bg-success text-light hotelOpt" name="hotelOpt">{{$hotel->hotel}}  ({{$hotel->precio}}€)</option>
                                        @else
                                            <option value="{{$hotel->id}}" class="hotelOpt" name="hotelOpt">{{$hotel->hotel}}  ({{$hotel->precio}}€)</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="fechaEntrada">Fecha Entrada al Hotel</label>
                            
                            @error('fechaEntrada')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="date" name="fechaEntrada" id="fechaEntrada" class="form-control" min="{{$fechaActual}}" value="{{old('fechaEntrada',  $reserva->estancia->fechaEntrada)}}" required>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="fechaSalida">Fecha Salida del Hotel</label>
                            
                            @error('fechaSalida')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <script>
                                $(document).ready(function(){
                                    $("#fechaSalida").click(function(){
                                        let valorFecha=document.getElementById('fechaEntrada').value;
                                        let etiqueta=document.getElementById('fechaSalida');
                                        etiqueta.setAttribute("min", valorFecha);
                                    });
                                });
                            </script>
                            
                            <input type="date" name="fechaSalida" id="fechaSalida" class="form-control" value="{{old('fechaSalida', $reserva->estancia->fechaSalida)}}" required>
                        </div> 
                        <br>
                        <div class="form-group">
                            <label for="transporte">Introduzca el medio de transporte con el que quiera viajar</label><br>
                            
                            @error('transporte')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            <select name="transporte" id="transporte" class="form-control"  required>
                                @foreach ($transportes as $transporte)
                                    @if ($transporte->esSostenible==0)
                                        <option value="{{$transporte->id}}" class="bg-success text-light">{{$transporte->nombre}}  ({{$transporte->precio}}€)</option>
                                    @else
                                        <option value="{{$transporte->id}}">{{$transporte->nombre}}  ({{$transporte->precio}}€)</option>
                                    @endif   
                                @endforeach
                            </select>
                            
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Modificar reserva</button>
                        </div>
                    </form>
                </div>
            
            @else
                <div class="card-body text center" style="padding:30px">
                    <h2>Usted no tiene permisospara  acceder a esta página</h2>
                    <br>
                    <script>
                        document.write('<a href="http://veru.proyecto/viajes">Volver</a>');
                    </script>
                </div>
            @endif
        </div>
        </div>
    </div>
@endsection