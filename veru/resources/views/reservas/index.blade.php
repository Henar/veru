@extends('layouts.master')

@section('titulo')
    Reservas
@endsection

@section('contenido')

    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
    
    <form method="POST">
        @csrf
        {{-- @php
            require_once ('../../../../App/Models/Lugar.php');
        @endphp --}}
        
        @if(empty($reservas))
            <h3>Todavía no ha realizado ninguna reserva</h3>
        @else
            <h3 style="text-align:center">Mostrando todas las reservas de {{Auth::user()->nombre}} {{Auth::user()->apellidos}}</h3>
            <div class="album py-5 bg-light">
                <div class="container">
                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    
                        @foreach($reservas as $reserva)
                            <div class="col">
                                <div class="card shadow-sm">
                                    <div class="card-body">
                                        <br>
                                    
                                        {{-- @php
                                            $lugarDestino=Lugar::all()->where('id', $reserva['destinoLugar_id']);
                                        @endphp --}}
                                        
                                        {{-- <div class="col-xs-12 col-sm-6 col-md-4 "> --}}
                                            <br>
                                            <a href="{{ route('reservas.show', $reserva)}}" style="text-decoration:none; color:black">  
                                                {{-- <h4>{{$reserva}}</h4> --}}
                                                {{-- <h4>{{$reserva['destinoLugar_id']}}</h4>  --}}        
                                                {{-- <h4>{{$reserva->transporte['nombre']}}</h4>  --}}
                                                {{-- <h4>{{$reserva->estancia['hotel']}}</h4> --}}
                                                {{-- <h4>{{$reserva->lugar['ciudad']}}</h4> --}}

                                                <h4>Lugar de destino de la reserva: {{$reserva->estancia->lugar->ciudad}}</h4>  
                                                @if(substr($reserva->estancia->lugar->imagen, 0, 8)==("https://"))
                                                    <img src="{{$reserva->estancia->lugar->imagen}}" alt="Imagen de {{$reserva->estancia->lugar->ciudad}}" style="margin-bottom:20px;padding:7px;" width="300px" class="rounded border border-3"/>
                                                @else
                                                    <img src="{{asset('assets/imagenes/')}}/{{$reserva->estancia->lugar->imagen}}" alt="Imagen de {{$reserva->estancia->lugar->ciudad}}" width="300px" class="rounded border border-3"/>
                                                @endif 
                                                   
                                                <br/>
                                                <h5>Fecha del inicio de la reserva: {{date("d-m-Y", strtotime($reserva->fechaReserva))}}</h5>
                                                <h5>Fecha en la que acaba la reserva: {{date("d-m-Y", strtotime($reserva->fechaFinReserva))}}</h5>
                                            </a>
                                        {{-- </div> --}}
                                    </div>
                                </div>
                            </div>  
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <br>
        {{-- {{$reservas->links()}} --}}
    </form>
    
@endsection