@extends('layouts.master')

@section('titulo')
    Reserva
@endsection

@section('contenido')
    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
    @php
        $estancia=$reserva->estancia;
        $transporte=$reserva->transporte;
        $fechaEntr=$estancia->fechaEntrada;
        $fechaSal=date("d-m-Y", strtotime($estancia->fechaSalida));
        $fechaEntr=date("d-m-Y", strtotime($fechaEntr));

        $fechaActual=date('Y-m-d');
    @endphp

    @if ($reserva->cliente_id== Auth::user()->id)
     
    <form method="POST" action="{{ route('reservas.destroy', $reserva)}}">
        @csrf
        @method('delete')
        {{-- <p>{{$reserva}}</p> --}}

        <h1 style="margin-left: 25%">{{$reserva->estancia->lugar->pais}} ({{$reserva->estancia->lugar->ciudad}})</h1>
        <div class="row">
            <br>
            <div class="col-sm-2">
                <p>Imagen del destino:</p>
                @if(substr($reserva->estancia->lugar->imagen, 0, 8)==("https://"))
                    <img src="{{$reserva->estancia->lugar->imagen}}" alt="Imagen de {{$reserva->estancia->lugar->ciudad}}" style="height:30%"/>
                @else
                    <img src="{{asset('assets/imagenes/')}}/{{$reserva->estancia->lugar->imagen}}" alt="Imagen de {{$reserva->estancia->lugar->ciudad}}" style="height:30%">
                @endif 
                <br><br> 
                <p>Imagen del hotel:</p>
                <img src="{{asset('assets/imagenes/')}}/{{$reserva->estancia->imagen}}" style="height:30%" alt="Imagen del hotel {{$reserva->estancia->hotel}}"/>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-9">

                <h5>Hospedaje:</h5>
                
                <h3>{{$estancia->hotel}}</h3>
                @if ($estancia->esSostenible==0)
                    <p class="text-success">El lugar escogido es sostenible</p>
                @endif
                <br>

                <h5>Medio de Transporte elegido:</h5> 
                
                <h3>{{$transporte->nombre}}</h3>
                @if (($transporte->esSostenible==0))
                <p class="text-success">El mediodetransporte elegido es sostenible</p>
                @endif
                <br>

                <h5>Precio:</h5>
                <h3>{{$reserva->totalReserva}}€</h3>

                <br>
                <h5>Descripción de la ciudad:</h5>
                
                @php
                    $parrafos=explode(PHP_EOL, $reserva->estancia->lugar->descripcion);
                @endphp
                @foreach ($parrafos as $parrafo)
                    <p>{{$parrafo}}</p>
                @endforeach

                <br>
                <h5>Fecha de entrada al hotel</h5>
                <h3>{{$fechaEntr}}</h3>
               
                <br>
                <h5>Fecha de salida del hotel</h3>
                <h3>{{$fechaSal}}</h3>

                <br/>
                {{-- @if($fechaEntr >$fechaActual) --}}
                    <a class="btn btn-warning" name="editar" style="margin-right: 7px" href = '{{ route('reservas.edit', $reserva)}}'>Editar</a>
                {{-- @endif --}}
                
                <a class="btn btn-light btn-outline-dark" name="volver" href = '{{ route('reservas.index')}}' style="margin-left: 7px">Volver a las reservas</a>
                @if($fechaEntr >$fechaActual)
                    <button class="btn btn-danger" name="borrar" style="margin-right: 7px" type="submit">Borrar reserva</button>
                @endif
            </div>
        </div>
    </form>
    @else
        <div class="row">
            <div class="offset-md-3 col-md-6">
                <div class="card">
                    <div class="card-body text center" style="padding:30px">
                        <h2>Usted no tiene permisospara  acceder a esta página</h2>
                        <br>
                        <script>
                            document.write('<a href="http://veru.proyecto/viajes">Volver</a>');
                        </script>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection