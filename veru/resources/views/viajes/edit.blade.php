@extends('layouts.master')

@section('titulo')
    Viajes
@endsection

@section('contenido')
    @php
    @endphp
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                @if (Auth::check() && Auth::user()->rol_id==1)
                    <div class="card-header text-center">
                        Modificar destino
                    </div>
                    <div class="card-body" style="padding:30px">
                        <form action="{{ route('viajes.update', $viaje)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <h6>Pais:</h6><h4> {{$viaje->pais}}</h4>
                            </div>
                            <br>
                            <div class="form-group">
                                <h6>Ciudad:</h6><h4> {{$viaje->ciudad}}</h4>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="precio">Introduzca nuevo precio para {{$viaje->ciudad}}</label><br>
                                
                                @error('precio')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <input type="number" name="precio" id="precio" class="form-control" step="0.01" value="{{old('precio', $viaje->precio)}}" required>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="descripcion">Introduzca una nueca descripción para la ciudad</label>
                                
                                @error('descripcion')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror
                                
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{old('descripcion', $viaje->descripcion)}}</textarea>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="imagen">Introduzca una nueva foto del destino</label><br>
                                
                                @error('imagen')
                                    <br>
                                    <small>*{{$message}}</small>
                                    <br>
                                @enderror

                                <input type="file" name="imagen" id="imagen"/>
                            </div>
                            <br>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Modificar destino</button>
                            </div>
                        </form>
                    </div>
                @else
                <div class="card-body text center" style="padding:30px">
                    <h2>No puede acceder a esta página</h2>
                    <br>
                    <script>
                        document.write('<a href="' + document.referrer + '">Volver</a>');
                    </script>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection