@extends('layouts.master')

@section('titulo')
    Viajes
@endsection

@section('contenido')
    @if (session ('mensaje'))
        <div class="alert alert-warning">{{session('mensaje')}}</div>
    @endif
    
    <form method="POST">
        @csrf
        {{-- <h3>Mostrando todos los posibles destinos</h3> --}}
        {{-- <div class="row"> --}}
            <div class="album py-5 bg-light">
                <div class="container">
                    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                        @foreach($viajes as $viaje)
                            <div class="col">
                                <div class="card shadow-sm">
                                    {{-- <div class="col-xs-12 col-sm-6 col-md-4 "> --}}
                                    <div class="card-body">
                                        <br>
                                        {{-- <a href="{{ route('viajes.show', $viaje)}}">                  --}}
                                        <h4 style="min-height:45px;margin:5px 0 10px 0" class="card-text">{{$viaje->ciudad}} ({{$viaje->pais}})</h4>
                                        {{-- <img src="{{asset('assets/imagenes/')}}/{{$viaje->imagen}}" style="width:406px;margin-bottom:20px;padding:7px;"/> --}}
                                                            {{-- class="rounded border border-3"/> --}}
                                        {{-- s --}}
                                        @if(substr($viaje->imagen, 0, 8)==("https://"))
                                            <img src="{{$viaje->imagen}}" alt="Imagen del destino {{$viaje->ciudad}}" width="100%" height="100%"/>
                                        @else
                                            <img src="{{asset('assets/imagenes/')}}/{{$viaje->imagen}}" alt="Imagen del destino {{$viaje->ciudad}}" width="100%" height="100%">
                                        @endif
                                        
                                        <br/><br/>

                                        @php
                                            if (strlen($viaje->descripcion)>175){
                                                echo "<p class='card-text'>". substr($viaje->descripcion, 0, 175). "...</p>";
                                            }else{
                                                echo "<p class='card-text'>$viaje->descripcion</p>";
                                            }
                                        @endphp
                                        {{-- </a> --}}
                                        <br/>
                                        <div class="d-flex align-items-end flex-column">
                                            <div class="btn-group" align="right">
                                                <a href="{{ route('viajes.show', $viaje)}}" class="btn btn-sm btn-outline-success" style="a:hover{background-color:green}">Conocer más</a>
                                                @if (Auth::check() && Auth::user()->rol_id==1)
                                                {{-- @if(($viaje->ciudad)=='Torrelavega') --}}
                                                    <a href = '{{ route('viajes.edit', $viaje)}}' class="btn btn-sm btn-warning">Editar</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        @endforeach
                    </div>
                </div>
            </div>

        <br>
        {{$viajes->links()}}
    </form>
    
    
@endsection
